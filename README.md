# Vim with plugins for work on rails applications

Vim is an advanced text editor that seeks to provide the power of the de-facto Unix editor 'Vi', with a more complete feature set.

## Setup

    cd ~
    git clone https://gitlab.com/dzaporozhets/dotvim.git
    ln -s $HOME/dotvim/.vim $HOME/.vim
    ln -s $HOME/dotvim/.vim/.vimrc $HOME/.vimrc